document.addEventListener('DOMContentLoaded', function () {
    const archivoInput = document.getElementById('archivoInput');
    const imgCont = document.getElementById('imgCont');

    archivoInput.addEventListener('change', function () {
        imgCont.innerHTML = ''; // Limpia el contenedor de imágenes

        const archivos = archivoInput.files;
        for (let i = 0; i < archivos.length; i++) {
            const archivo = archivos[i];

            if (archivo.type.startsWith('image')) {
                const imagen = document.createElement('img');
                imagen.src = URL.createObjectURL(archivo);
                imagen.classList.add('imagen');
                imgCont.appendChild(imagen);
            }
        }
    });
});
