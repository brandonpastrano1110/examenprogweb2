document.getElementById("getTabla").addEventListener("click", function () {
    const numeroTabla = parseInt(document.getElementById("selectTabla").value);
    const tablaResultado = document.getElementById("res");
    tablaResultado.innerHTML = "";

    if (numeroTabla >= 1 && numeroTabla <= 10) {
        for (let i = 1; i <= 10; i++) {
            const multiplicacion = numeroTabla * i;
            const fila = document.createElement("div");
            fila.classList.add("fila");

            fila.appendChild(createImageElement(`${numeroTabla}.png`, numeroTabla));
            fila.appendChild(createImageElement("x.png", "x"));
            fila.appendChild(createImageElement(`${i}.png`, i));
            fila.appendChild(createImageElement("=.png", "="));

            const resultadoStr = multiplicacion.toString();
            for (let j = 0; j < resultadoStr.length; j++) {
                fila.appendChild(createImageElement(`${resultadoStr.charAt(j)}.png`, resultadoStr.charAt(j)));
            }

            tablaResultado.appendChild(fila);
        }
    }
});

function createImageElement(src, alt) {
    const img = document.createElement("img");
    img.src = `/img/${src}`; 
    img.alt = alt;
    return img;
}
