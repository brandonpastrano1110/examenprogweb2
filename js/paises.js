document.addEventListener('DOMContentLoaded', function () {
    const selectPais = document.getElementById('selectPais');
    const paisInfo = document.getElementById('paisInfo');

    fetch('/js/paises.json')
        .then(response => response.json())
        .then(data => {
            selectPais.addEventListener('input', function () {
                const selectedValue = selectPais.value;
                const index = selectedValue - 1;

                if (index >= 0 && index < data.length) {
                    const pais = data[index];
                    paisInfo.children[0].textContent = `País: ${pais.pais}`;
                    paisInfo.children[1].textContent = `Nombre de la moneda: ${pais.moneda}`;
                    paisInfo.children[2].textContent = `Valor de la moneda: ${pais.valor_cambio}`;
                }
            });
        });
});
